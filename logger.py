import logging
import sys


LOGGER = logging.getLogger("parser")
FILE_NAME = "parser.log"

FILE_HANDLER = logging.FileHandler(FILE_NAME, encoding="utf-8")
FILE_HANDLER.setLevel(logging.DEBUG)

STREAM_HANDLER = logging.StreamHandler(sys.stderr)
STREAM_HANDLER.setLevel(logging.INFO)

FORMATTER_DEBUG = logging.Formatter(
    "%(levelname)-10s fun(%(funcName)s) :%(lineno)d - %(message)s"
)
FORMATTER_INFO = logging.Formatter("%(levelname)-10s %(asctime)s - %(message)s")

STREAM_HANDLER.setFormatter(FORMATTER_INFO)
FILE_HANDLER.setFormatter(FORMATTER_DEBUG)

LOGGER.addHandler(STREAM_HANDLER)
LOGGER.addHandler(FILE_HANDLER)
LOGGER.setLevel(logging.DEBUG)
