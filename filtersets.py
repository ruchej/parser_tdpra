import settings as s
from utils import path, get_url_json


def get_filters(selected_id):
    """Возвращает данные фильтра, относящиеся к определённой рубрики"""
    
    url = path(s.URLS["filters"].format(selected_id))
    response = get_url_json(url)
    return response


def get_sets(filter, manufacturers):
    """Получаем id-шники производителей"""

    obj = get_filters(filter)["objects"]
    manufactures_id = []
    for i in obj:
        if i.get("slug", None) in manufacturers:
            manufactures_id.append(i["id"])
    return manufactures_id
