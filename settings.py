import pymongo


MONGO = pymongo.MongoClient("mongodb://localhost:27017/")
TDPRA_DB = MONGO["tdpra"]

HOST = "https://www.tdpra.ru"

URLS = {
    "filters": "/shop/rubricator/selected/{}",
    "products": "/shop/products/set/{}",
    "category_url_root": "/shop/category/",
    "ldsp": "/shop/products/path/stroitelnye-materialy/ldsp/set/{}",
    "bands_pvc": "/shop/products/path/kromka/kromka-pvh/",
}

MANUFACTURERS_LDSP = ["ldsp", "egger", "kronoshpan", "dyatkovo"]
MANUFACTURERS_BAND = [
    "kromka-pvh",
    "egger",
    "kronoplast",
    "dyatkovo",
    "lamarty",
    "arket",
    "vk",
    "rehau",
    "galoplast",
    "karasyov",
    "agt",
]
LDSP_FILTER = 159
BAND_FILTER = 156

KEY_MANUFACTURER = "base/197be151-dd50-11e6-a062-bcaec52823f5"
KEY_QUALITY = "category/stroitelnye-materialy/vid/ldsp/kachestvo"

KEY_THICKNESS_LDSP = "category/stroitelnye-materialy/vid/ldsp/tolshina"
KEY_SIZE_LDSP = "category/stroitelnye-materialy/vid/ldsp/razmer"

KEY_THICKNESS_BAND = (
    "category/komplektuyushie-k-mebeli/vid/kromka/vid/kromka-pvh/tolshina"
)
KEY_WIDTH_BAND = "category/komplektuyushie-k-mebeli/vid/kromka/vid/kromka-pvh/shirina"
QUALITY_VALUE = {"Обычное": 0, "Влагостойкое": 1, "Глянец": 2, "Лакированное": 3}
