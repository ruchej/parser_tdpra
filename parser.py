import json
import settings as s
import filtersets
import utils
import logging
import logger


LOGGER = logging.getLogger("parser")


def get_ldsp():
    """Возвращает данные по лдсп"""

    def _get_short_desc(obj):
        """Получаем хаактеристики из краткого описания"""
        short_characteristics = obj["short_characteristics"]
        LOGGER.debug(f"{obj}")
        thickness = utils.get_number(short_characteristics.get(s.KEY_THICKNESS_LDSP, {"v": "0"})["v"])
        length, width = utils.find_number(short_characteristics.get(s.KEY_SIZE_LDSP, {"v": "0x0"})["v"])
        manufacturer = short_characteristics.get(s.KEY_MANUFACTURER, {"v": None})["v"]
        quality = "Обычное"
        if "short_marks" in obj.keys():
            if s.KEY_QUALITY in obj["short_marks"]:
                short_marks = obj["short_marks"]
                quality = short_marks[s.KEY_QUALITY]["v"]
        return utils.str_to_num(thickness), utils.str_to_num(length), utils.str_to_num(width), manufacturer, quality

    def _parse(url, limit=12, offset=0):
        """Парсинг одной группы"""
        LOGGER.debug(f"{url}?{limit=}&{offset=}")
        response = utils.get_url_json(url, limit, offset)
        total_count = response["meta"]["total_count"]
        while offset < total_count:
            for obj in response["objects"]:
                if obj.get("variants", 1) > 1:
                    url_grp = utils.path(obj["catalog_base_uri"])
                    _parse(url_grp)
                else:
                    thickness, length, width, manufacturer, quality = _get_short_desc(obj)
                    data = {
                        "name": obj["name"],
                        "article": obj["sku"],
                        "price": obj["price"],
                        "quantity": obj["quantity"],
                        "thickness": thickness,
                        "length": length,
                        "width": width,
                        "manufacturer": manufacturer,
                        "quality": quality
                    }
                    LOGGER.debug(f"Вносим данные {data=}")
                    table_ldsp.insert_one(data)
            limit = response["meta"]["limit"]
            offset = limit + response["meta"]["offset"]
            LOGGER.debug(f"{url}?{limit=}&{offset=}")
            response = utils.get_url_json(url, limit, offset)
        LOGGER.debug("Обработана группа")
        return

    table_ldsp = utils.db_collection_add("ldsp")
    sets = filtersets.get_sets(s.LDSP_FILTER, s.MANUFACTURERS_LDSP)
    sets_to_str = [str(i) for i in sets]
    url = utils.path(s.URLS["ldsp"]).format(";".join(sets_to_str))
    _parse(url)


def get_bands():
    """Возвращает список кромок ПВХ"""

    def _get_short_desc(obj):
        """Получаем хаактеристики из краткого описания"""
        short_characteristics = obj["short_characteristics"]
        LOGGER.debug(f"{obj}")
        thickness = utils.get_number(short_characteristics.get(s.KEY_THICKNESS_BAND, {"v": "0"})["v"])

        width = utils.get_number(short_characteristics.get(s.KEY_WIDTH_BAND, {"v": "0"})["v"])

        manufacturer = short_characteristics.get(s.KEY_MANUFACTURER, {"v": None})["v"]
        return utils.str_to_num(thickness), utils.str_to_num(width), manufacturer

    def _parse(url, limit=12, offset=0):
        """Парсинг одной группы"""
        LOGGER.debug(f"{url}?{limit=}&{offset=}")
        response = utils.get_url_json(url, limit, offset)
        total_count = response["meta"]["total_count"]
        while offset < total_count:
            for obj in response["objects"]:
                if obj.get("variants", 1) > 1:
                    url_grp = utils.path(obj["catalog_base_uri"])
                    _parse(url_grp)
                else:
                    thickness, width, manufacturer = _get_short_desc(obj)
                    data = {
                        "name": obj["name"],
                        "article": obj["sku"],
                        "price": obj["price"],
                        "quantity": obj["quantity"],
                        "thickness": thickness,
                        "width": width,
                        "manufacturer": manufacturer,
                    }
                    LOGGER.debug(f"Вносим данные {data=}")
                    table_band.insert_one(data)
            limit = response["meta"]["limit"]
            offset = limit + response["meta"]["offset"]
            LOGGER.debug(f"{url}?{limit=}&{offset=}")
            response = utils.get_url_json(url, limit, offset)
        LOGGER.debug("Обработана группа")
        return

    table_band = utils.db_collection_add("band")
    sets = filtersets.get_sets(s.BAND_FILTER, s.MANUFACTURERS_BAND)
    sets_to_str = [str(i) for i in sets]
    url = utils.path(s.URLS["bands_pvc"]).format(";".join(sets_to_str))
    _parse(url)


# get_bands()
get_ldsp()
