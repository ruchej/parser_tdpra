import settings as s
import requests
import re


def path(url):
    """Возвращает полный путь добавив хост к вхдному url"""

    return s.HOST + url


def get_url_json(url, limit=12, offset=0):
    """Отправляет GET запрос на url с параметрами limit, offset и format=json"""

    return requests.get(
        url, params={"format": "json", "limit": limit, "offset": offset}
    ).json()


def db_collection_add(name):
    """Создаёт коллекцию в БД. Если такая уже есть, то удаляет и создаёт пустую"""

    collist = s.TDPRA_DB.list_collection_names()
    if name in collist:
        s.TDPRA_DB[name].drop()
    return s.TDPRA_DB[name]


def check_for_list(val):
    """Если значние является списком, возвращаем первое значение"""
    if type(val) is list:
        return val[0]
    else:
        return val


def find_number(val):
    """Возвращает числа, найденные в строке"""
    return re.findall(r"[+-]?[0-9]*[.|,]?[0-9]+", val)

def get_number(val):
    """Возвращает число из строки"""
    val = check_for_list(val)
    val = find_number(val)
    val = check_for_list(val)
    val = val.replace(",", ".")
    return val


def str_to_num(val):
    if val.isdigit():
        return int(val)
    else:
        return float(val)
